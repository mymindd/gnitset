import mongoengine as me
import datetime


class GroupMember(me.EmbeddedDocument):
    group = me.ReferenceField('UserGroup',
                              dbref=True)
    granter = me.ReferenceField('User', dbref=True)
    rrule = me.EmbeddedDocumentField('Rrule')
    started_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)
    expired_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)

    def check_rrule(self):
        if (datetime.datetime.now() < self.started_date or
                datetime.datetime.now() > self.expired_date):
            return False
        from dateutil.rrule import rrule, DAILY
        h_start, m_start, _ = self.rrule.start_time.split(':')
        expected_datetime = list(rrule(freq=DAILY,
                                       dtstart=datetime.datetime.combine(
                                           datetime.date.today(),
                                           datetime.time(0, 0)),
                                       byweekday=self.rrule.days,
                                       count=1,
                                       byhour=int(h_start),
                                       byminute=int(m_start),
                                       bysecond=0))
        h_end, m_end, _ = self.rrule.end_time.split(':')
        exp_datetime = datetime.datetime.combine(datetime.date.today(),
                                                 datetime.time(int(h_end),
                                                               int(m_end)))
        # print(expected_datetime)
        for dt in expected_datetime:

            if (datetime.datetime.now() >= dt and
                    datetime.datetime.now() <= exp_datetime):
                return True
        return False
