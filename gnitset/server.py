import json
import asyncio
from gnitset import models


class ControllerServer:
    def __init__(self, settings):
        self.settings = settings
        self.running = False
        self.command_queue = asyncio.Queue()

    async def process_command(self):
        while self.running:
            data = await self.command_queue.get()
            door = models.Door.objects.get(id=data['door_id'])
            user = models.User.objects.get(id=data['user_id'])
            user_group = models.UserGroup.objects.get(id=data['user_group_id'])
            door_auth = door.get_door_auth()
            if not user_group.is_member(user):
                continue
            # logger.debug(user_group.name)
            if not door_auth.is_authority(user_group):
                continue
            topic = f'pichayon.node_controller.{door.device_id}'
            command = dict(device_id=door.device_id, action='open')
            print('send')
            await self.nc.publish(
                        topic,
                        json.dumps(command).encode())
