from unittest.mock import MagicMock, patch
import unittest
from gnitset import rrule
import datetime
from collections import namedtuple


class TestsRrule(unittest.TestCase):

    def setUp(self):
        self.group_member = rrule.GroupMember()
        self.group_member.started_date = datetime.datetime.combine(
                datetime.date(2019, 11, 28),
                datetime.time(0, 0))
        self.group_member.expired_date = datetime.datetime.combine(
                datetime.date(2019, 12, 6),
                datetime.time(0, 0))
        r_rule = {'start_time': '8:00:00',
                  'end_time': '17:00:00',
                  'days': [0, 1, 2, 3, 4]}  # Mon-Fri
        self.group_member.rrule = namedtuple('rrule', r_rule.keys())(*r_rule.values())

    def tearDown(self):
        self.group_member = None

    def assert_data_false(self):
        result = self.group_member.check_rrule()
        self.assertFalse(result)

    def assert_data_true(self):
        result = self.group_member.check_rrule()
        self.assertTrue(result)

    def datetime_combine(self, h_end, m_end):
        datetime_com = datetime.datetime.combine(
                datetime.date(2019, 11, 29),
                datetime.time(
                    int(h_end),
                    int(m_end)))
        return datetime_com

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_before_started_date_should_be_false(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 11, 27),
                datetime.time(12, 0))
        self.assert_data_false()

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_after_expired_date_should_be_false(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 12, 7),
                datetime.time(12, 0))
        self.assert_data_false()

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_in_days_but_not_in_time_should_be_false(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 11, 29),
                datetime.time(7, 0))
        h_end, m_end, _ = self.group_member.rrule.end_time.split(':')
        datetime_mock.datetime.combine.side_effect = [self.datetime_combine(0, 0),
                                                      self.datetime_combine(h_end, m_end)]
        self.assert_data_false()

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_in_days_n_in_12_00_should_be_true(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 11, 29),
                datetime.time(12, 0))
        h_end, m_end, _ = self.group_member.rrule.end_time.split(':')
        datetime_mock.datetime.combine.side_effect = [self.datetime_combine(0, 0),
                                                      self.datetime_combine(h_end, m_end)]
        self.assert_data_true()

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_in_days_n_in_8_00_should_be_true(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 11, 29),
                datetime.time(8, 0))
        h_end, m_end, _ = self.group_member.rrule.end_time.split(':')
        datetime_mock.datetime.combine.side_effect = [self.datetime_combine(0, 0),
                                                      self.datetime_combine(h_end, m_end)]
        self.assert_data_true()

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_in_days_n_in_17_00_should_be_true(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 11, 29),
                datetime.time(17, 0))
        h_end, m_end, _ = self.group_member.rrule.end_time.split(':')
        datetime_mock.datetime.combine.side_effect = [self.datetime_combine(0, 0),
                                                      self.datetime_combine(h_end, m_end)]
        self.assert_data_true()

    @patch('gnitset.rrule.datetime')
    def test_datetime_now_not_in_days_but_in_time_should_be_false(self, datetime_mock):
        datetime_mock.datetime.now.return_value = datetime.datetime.combine(
                datetime.date(2019, 11, 30),
                datetime.time(12, 0))
        h_end, m_end, _ = self.group_member.rrule.end_time.split(':')
        datetime_mock.datetime.combine.side_effect = [self.datetime_combine(0, 0),
                                                      self.datetime_combine(h_end, m_end)]
        self.assert_data_false()


if __name__ == "__main__":
    unittest.main()
