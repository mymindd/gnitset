from unittest.mock import MagicMock, patch
import unittest
from gnitset import door_auth


class TestsDoorAuth(unittest.TestCase):

    def assert_data(self, expected):
        door = door_auth.Door()
        result = door.get_door_auth()
        self.assertEqual(expected, result)

    @patch('gnitset.door_auth.models')
    def test_empty_data_should_be_none(self, models_mock):
        # mongomock.Document.return_value = 'This is Mock of documents'
        self.assert_data(None)

    @patch('gnitset.door_auth.models')
    def test_mock_door_group_should_be_none(self, models_mock):
        door_group = MagicMock()
        door_group.is_member.return_value = False
        models_mock.DoorGroup.objects.return_value = [door_group]
        self.assert_data(None)

    @patch('gnitset.door_auth.models')
    def test_mock_door_group_with_ismember_no_door_auth_should_be_none(self, models_mock):
        door_group = MagicMock()
        door_group.is_member.return_value = True
        models_mock.DoorGroup.objects.return_value = [door_group]
        models_mock.DoorAuthorizations.objects().first.return_value = []
        self.assert_data(None)

    @patch('gnitset.door_auth.models')
    def test_mock_door_group_with_ismember_should_be_mock(self, models_mock):
        door_group = MagicMock()
        door_group.is_member.return_value = True
        doorauth = MagicMock()
        models_mock.DoorGroup.objects.return_value = [door_group]
        models_mock.DoorAuthorizations.objects().first.return_value = doorauth
        self.assert_data(doorauth)

    @patch('gnitset.door_auth.models')
    def test_mock_2door_group_with_ismember_1st_group_should_be_firstmock(self, models_mock):
        door_group1 = MagicMock()
        door_group2 = MagicMock()
        door_group1.is_member.return_value = True
        door_group2.is_member.return_value = False
        doorauth = MagicMock()
        models_mock.DoorGroup.objects.return_value = [door_group1, door_group2]
        models_mock.DoorAuthorizations.objects().first.return_value = doorauth
        self.assert_data(doorauth)

    @patch('gnitset.door_auth.models')
    def test_mock_2door_group_with_ismember_2nd_group_should_be_secondmock(self, models_mock):
        door_group1 = MagicMock()
        door_group2 = MagicMock()
        door_group1.is_member.return_value = False
        door_group2.is_member.return_value = True
        doorauth = MagicMock()
        models_mock.DoorGroup.objects.return_value = [door_group1, door_group2]
        models_mock.DoorAuthorizations.objects().first.return_value = doorauth
        self.assert_data(doorauth)

    @patch('gnitset.door_auth.models')
    def test_mock_2door_group_without_ismember_group_should_be_none(self, models_mock):
        door_group1 = MagicMock()
        door_group2 = MagicMock()
        door_group1.is_member.return_value = False
        door_group2.is_member.return_value = False
        doorauth = MagicMock()
        models_mock.DoorGroup.objects.return_value = [door_group1, door_group2]
        models_mock.DoorAuthorizations.objects().first.return_value = doorauth
        self.assert_data(None)

    @patch('gnitset.door_auth.models')
    def test_mock_2door_group_with_ismember_all_group_should_be_firstmock(self, models_mock):
        door_group1 = MagicMock()
        door_group2 = MagicMock()
        door_group1.is_member.return_value = True
        door_group2.is_member.return_value = True
        doorauth = MagicMock()
        models_mock.DoorGroup.objects.return_value = [door_group1, door_group2]
        models_mock.DoorAuthorizations.objects().first.return_value = doorauth
        self.assert_data(doorauth)


if __name__ == "__main__":
    unittest.main()
