from unittest.mock import MagicMock, patch, AsyncMock
import unittest
from gnitset import server
import asyncio
import time


class TestsControllerServer(unittest.IsolatedAsyncioTestCase):

    async def set_time_out(self):
        time.sleep(3)
        self.sv.running = False

    async def assert_not_call(self):
        loop = asyncio.get_event_loop()
        task2 = loop.create_task(self.sv.process_command())
        task1 = loop.create_task(self.set_time_out())
        try:
            loop.run_forever()
        except Exception as e:
            print(e)

        self.sv.nc.publish.assert_not_called()

    async def assert_call_once(self):
        loop = asyncio.get_event_loop()
        task2 = loop.create_task(self.sv.process_command())
        task1 = loop.create_task(self.set_time_out())
        try:
            loop.run_forever()
        except Exception as e:
            print(e)
        self.sv.nc.publish.assert_called_once()

    async def asyncSetUp(self):
        self.sv = server.ControllerServer(MagicMock())
        self.sv.running = True
        self.sv.nc = AsyncMock()

    async def asyncTearDown(self):
        self.sv = None

    async def test_ug_with_not_member_should_be_not_call(self):
        self.sv.command_queue = AsyncMock()
        self.sv.command_queue.get.return_value = MagicMock()
        door = MagicMock()
        door.get_door_auth.return_value = MagicMock()

        models_mock = MagicMock()
        server.models = models_mock

        models_mock.Door.objects.get.return_value = door
        ug = MagicMock()
        ug.is_member.return_value = False
        models_mock.UserGroup.objects().get.return_value = ug
        models_mock.User.objects.get.return_value = MagicMock()
        await self.assert_not_call()
 
    async def test_ug_with_is_member_should_be_not_call(self):
        self.sv.command_queue = AsyncMock()
        self.sv.command_queue.get.return_value = MagicMock()
        door_auth = MagicMock()
        door_auth.is_authority.return_value = True
        door = MagicMock()
        door.get_door_auth.return_value = door_auth

        models_mock = MagicMock()
        server.models = models_mock

        models_mock.Door.objects.get.return_value = door
        ug = MagicMock()
        ug.is_member.return_value = True
        models_mock.UserGroup.objects().get.return_value = ug
        models_mock.User.objects.get.return_value = MagicMock()
        await self.assert_not_call()
 

if __name__ == "__main__":
    unittest.main()
