from unittest.mock import MagicMock, patch
import unittest
from gnitset import data_resource


class TestDataResource(unittest.TestCase):

    def assert_data(self, expected):
        resource = data_resource.DataResourceManager()
        result = resource.get_authorization_data(device_id=None)
        self.assertEqual(expected, result)

    def assert_data_in(self, expected):
        resource = data_resource.DataResourceManager()
        result = resource.get_authorization_data(device_id=None)
        self.assertIn(expected, result.values())

    @patch('gnitset.data_resource.models')
    def test_empty_data_should_be_none(self, models_mock):
        models_mock.DoorGroup.objects.return_value = []
        self.assert_data({})

    @patch('gnitset.data_resource.models')
    def test_have_dg_but_havent_ug_should_be_none(self, models_mock):
        door_group = MagicMock()
        door_group.search_device_id.return_value = True
        models_mock.DoorGroup.objects.return_value = [door_group]
        door_auth = MagicMock()
        door_auth.user_group = []
        models_mock.DoorAuthorizations.objects().first.return_value = door_auth
        self.assert_data({})

    @patch('gnitset.data_resource.models')
    def test_have_dg_n_ug_but_ug_no_auth_should_be_none(self, models_mock):
        door_group = MagicMock()
        door_group.search_device_id.return_value = True
        models_mock.DoorGroup.objects.return_value = [door_group]
        door_auth = MagicMock()
        door_auth.is_authority.return_value = False
        user_group = MagicMock()
        door_auth.user_group = [user_group]
        models_mock.DoorAuthorizations.objects().first.return_value = door_auth
        self.assert_data({})

    @patch('gnitset.data_resource.models')
    def test_have_dg_ug_n_ug_is_auth_should_be_have_mock_name(self, models_mock):
        door_group = MagicMock()
        door_group.search_device_id.return_value = True
        models_mock.DoorGroup.objects.return_value = [door_group]
        door_auth = MagicMock()
        door_auth.is_authority.return_value = True
        user_group = MagicMock()
        group = MagicMock()
        group.name = 'mock_name'
        group.members = []
        user_group.group = group
        door_auth.user_group = [user_group]
        models_mock.DoorAuthorizations.objects().first.return_value = door_auth
        self.assert_data_in(group.name)

    @patch('gnitset.data_resource.models')
    def test_have_dg_ug_n_ug_is_auth_should_be_have_mock_name(self, models_mock):
        door_group = MagicMock()
        door_group.search_device_id.return_value = True
        models_mock.DoorGroup.objects.return_value = [door_group]
        door_auth = MagicMock()
        door_auth.is_authority.return_value = True
        user_group1 = MagicMock()
        user_group2 = MagicMock()
        group = MagicMock()
        group.name = 'mock_name1'
        member = MagicMock()
        member.user.username = 'mock_username'
        group.members = [member]
        user_group1.group = group
        group.name = 'mock_name2'
        user_group2.group = group
        door_auth.user_group = [user_group1, user_group2]
        models_mock.DoorAuthorizations.objects().first.return_value = door_auth
        self.assert_data_in('mock_name2')


if __name__ == '__main__':
    unittest.main()
